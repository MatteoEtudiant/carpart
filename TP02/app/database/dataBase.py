from fastapi import FastAPI
import mysql.connector

app = FastAPI()

# Connexion à la base de données MySQL
connection = mysql.connector.connect(
    host='localhost',
    user='root',
    port=3306,
    passwd='Getairborne@156',
    database='CarPart'
)

cursor = connection.cursor()

# Script pour créer la base de données et les tables si elles n'existent pas
try:
    cursor.execute("CREATE DATABASE IF NOT EXISTS CarPart")
    connection.database = 'CarPart'
    
    # Créez la table Clients si elle n'existe pas
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS Clients (
            id INT AUTO_INCREMENT PRIMARY KEY,
            nom VARCHAR(255) NOT NULL,
            prenom VARCHAR(255) NOT NULL,
            email VARCHAR(255) NOT NULL,
            nb_commandes INT NOT NULL
        )
    """)
    
    connection.commit()
except mysql.connector.Error as err:
    print(f"Erreur MySQL : {err}")
finally:
    cursor.close()
    connection.close()