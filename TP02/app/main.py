from fastapi import FastAPI, HTTPException, Depends
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel
import mysql.connector

app = FastAPI()

# Connexion à la base de données MySQL
connection = mysql.connector.connect(
    host='mysql',
    user='root',
    port=3306,
    passwd='Getairborne@156',
    database='CarPart'
)

cursor = connection.cursor()

# Script pour créer la base de données et les tables si elles n'existent pas
try:
    cursor.execute("CREATE DATABASE IF NOT EXISTS CarPart")
except mysql.connector.Error as err:
    print(f"Erreur MySQL : {err}")

cursor.execute("USE CarPart")

# Créez la table Clients si elle n'existe pas
cursor.execute("""
    CREATE TABLE IF NOT EXISTS Clients (
        id INT AUTO_INCREMENT PRIMARY KEY,
        nom VARCHAR(255) NOT NULL,
        prenom VARCHAR(255) NOT NULL,
        email VARCHAR(255) NOT NULL,
        nb_commandes INT NOT NULL
    )
""")

connection.commit()

class Client(BaseModel):
    nom: str
    prenom: str
    email: str
    nb_commandes: int

@app.get("/")
async def root():
    return {"message": "Bienvenue sur l'API"}

@app.get("/clients/")
async def allClients():
    query = "SELECT * FROM Clients"
    cursor.execute(query)
    clients = cursor.fetchall()

    clients_list = []
    for client in clients:
        client_dict = {
            "id": client[0],
            "nom": client[1],
            "prenom": client[2],
            "email": client[3],
            "nb_commandes": client[4]
        }
        clients_list.append(client_dict)

    return JSONResponse(content=jsonable_encoder(clients_list))

@app.get("/clients/{client_id}")
async def getClient(client_id: int):
    query = "SELECT * FROM Clients WHERE id = %s"
    cursor.execute(query, (client_id,))
    client = cursor.fetchone()

    if not client:
        raise HTTPException(status_code=404, detail="Client not found")

    client_dict = {
        "id": client[0],
        "nom": client[1],
        "prenom": client[2],
        "email": client[3],
        "nb_commandes": client[4]
    }

    return JSONResponse(content=jsonable_encoder(client_dict))

@app.post("/clients/add")
async def addClient(client: Client):
    query = "INSERT INTO Clients (nom, prenom, email, nb_commandes) VALUES (%s, %s, %s, %s)"
    data = (client.nom, client.prenom, client.email, client.nb_commandes)
    cursor.execute(query, data)
    connection.commit()

    inserted_client = {
        "nom": client.nom,
        "prenom": client.prenom,
        "email": client.email,
        "nb_commandes": client.nb_commandes
    }

    return JSONResponse(content=jsonable_encoder(inserted_client))

@app.put("/clients/update/{client_id}")
async def updateClient(client_id: int, updated_data: Client):
    query = "UPDATE Clients SET nom = %s, prenom = %s, email = %s, nb_commandes = %s WHERE id = %s"
    data = (updated_data.nom, updated_data.prenom, updated_data.email, updated_data.nb_commandes, client_id)
    cursor.execute(query, data)
    connection.commit()

    updated_client = {
        "id": client_id,
        "nom": updated_data.nom,
        "prenom": updated_data.prenom,
        "email": updated_data.email,
        "nb_commandes": updated_data.nb_commandes
    }

    return JSONResponse(content=jsonable_encoder(updated_client))

@app.delete("/clients/delete/{client_id}")
async def deleteClient(client_id: int):
    query = "DELETE FROM Clients WHERE id = %s"
    cursor.execute(query, (client_id,))
    connection.commit()

    deleted_client = {
        "id": client_id
    } 

    return JSONResponse(content=jsonable_encoder(deleted_client))
