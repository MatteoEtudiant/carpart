from pymongo import MongoClient

# Connexion à la base de données MongoDB
client = MongoClient("mongodb://localhost:27017/")
database = client["CarPart"]
collection = database["produits"]

# Vérifiez si la collection "produits" existe déjà
if "produits" not in database.list_collection_names():
    # Créez la collection "produits"
    database.create_collection("produits")

# Affichez les collections de la base de données pour vérification
print(database.list_collection_names())
