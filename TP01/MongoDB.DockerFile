# Utilisez une image officielle MongoDB
FROM mongo:latest


# Copiez le script de création de la base de données dans le conteneur
COPY dataBase.py /docker-entrypoint-initdb.d/

# Exposez le port MongoDB
EXPOSE 27017

# Créez le répertoire de logs et définissez les permissions
RUN mkdir -p /var/log/ && chown -R mongodb:mongodb /var/log/

# Commande pour lancer MongoDB
CMD ["mongod", "--logpath", "/var/log/mongodb.log"]  
