from pymongo import MongoClient
from fastapi import FastAPI, Depends, HTTPException
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel
from bson import ObjectId

client = MongoClient("mongodb://localhost:27017/")
database = client["CarPart"]
collection = database["produits"]
app = FastAPI()

class Product(BaseModel):
    nom: str
    descriptif: str
    quantite: int
    prix: float

class UpdateProduct(BaseModel):
    descriptif: str = None
    quantite: int = None

@app.get("/")
async def root():
    return {"message": "Bienvenue sur l'api"}

@app.get("/produits/")
async def allProducts():
    produits = collection.find({})
    produits_list = []
    for produit in produits:
        produit["_id"] = str(produit["_id"])
        produits_list.append(produit)

    produits_json = jsonable_encoder(produits_list)

    return JSONResponse(content=produits_json)

@app.get("/produits/{product_name}")
async def getProduct(product_name: str):
    product = collection.find_one({"nom": product_name})

    if not product:
        raise HTTPException(status_code=404, detail="Product not found")

    product["_id"] = str(product["_id"])
    return JSONResponse(content=jsonable_encoder(product))

@app.post("/produits/add")
async def addProduct(product: Product):
    product_dict = product.dict()
    result = collection.insert_one(product_dict)
    inserted_product = collection.find_one({"nom": product.nom})
    
    if not inserted_product:
        raise HTTPException(status_code=500, detail="Failed to insert the product")

    inserted_product["_id"] = str(inserted_product["_id"])
    return JSONResponse(content=jsonable_encoder(inserted_product))

@app.put("/produits/update/{product_name}")
async def updateProduct(product_name: str, update_data: UpdateProduct):
    existing_product = collection.find_one({"nom": product_name})

    if not existing_product:
        raise HTTPException(status_code=404, detail="Product not found")

    update_data_dict = update_data.dict(exclude_unset=True)

    if update_data_dict:
        collection.update_one({"nom": product_name}, {"$set": update_data_dict})

    updated_product = collection.find_one({"nom": product_name})
    updated_product["_id"] = str(updated_product["_id"])

    return JSONResponse(content=jsonable_encoder(updated_product))

@app.delete("/produits/delete/{product_name}")
async def deleteProduct(product_name: str):
    deleted_product = collection.find_one_and_delete({"nom": product_name})

    if not deleted_product:
        raise HTTPException(status_code=404, detail="Product not found")

    deleted_product["_id"] = str(deleted_product["_id"])
    return JSONResponse(content=jsonable_encoder(deleted_product))

