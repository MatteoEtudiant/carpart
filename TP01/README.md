Pour lancer l'api il faut :

Build les deux fichiers DockerFile :
    - On peut les builds avec les commandes :
        - docker build -t imageapi -f API.DockerFile .
        - docker build -t imagemongodb -f MongoDB.DockerFile
Ensuite il faut executé dans l'ordre ces images, donc tout d'abord la bdd :
    - docker run --network="host" imagemongodb
Et ensuite l'api :
    docker run -p 8000:8000 --network="host" imageapi